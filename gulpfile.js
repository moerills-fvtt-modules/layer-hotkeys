const gulp = require('gulp');
const concat = require('gulp-concat'):
const less = require('gulp-less'):
const util = require('./util');

const moduleName = util.getModuleName();

const SCRIPTS = ['scripts/**/*.js']
function concatScripts() {
  return gulp.src(SCRIPTS).pipe(concat(moduleName + '.js'))
    .pipe(gulp.dest('scripts/'));
}
const js = gulp.series(concatScripts);

/* ----------------------------------------- */
/*  Compile LESS
/* ----------------------------------------- */

// Compile LESS CSS
function compileLess() {
  return gulp.src('less/*.less')
    .pipe(less())
    .pipe(concat(moduleName + '.css'))
    .pipe(gulp.dest('css/'));
}
const css = gulp.series(compileLess);

function watchUpdates() {
  // gulp.watch(SCRIPTS, js);
  gulp.watch(LESS, css);
}

exports.default = gulp.series(
  gulp.parallel(js, css),
  watchUpdates
);

exports.js = js;
exports.css = css;