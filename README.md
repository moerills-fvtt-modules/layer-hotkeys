# Layer Hotkeys  
![FVTT Version](https://img.shields.io/badge/FVTT-%3E%3D%200.5.4-critical)  
[![MIT-License](https://img.shields.io/badge/License-MIT-black?style=flat-square)](https://gitlab.com/moerills-fvtt-modules/layer-hotkeys/raw/master/LICENSE) 
[![PayPal](https://img.shields.io/badge/Buy%20Me%20A%20Coffee-PayPal-blue?style=flat-square)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FYZ294SP2JBGS&source=url)  

# This repository moved to GitHub!
https://github.com/Moerill/Layer-Hotkeys
# Attribution
This module uses [settings-extender v1.0.1](https://gitlab.com/foundry-azzurite/settings-extender) by Azzurite licensed under the [LGPLv3](https://choosealicense.com/licenses/lgpl-3.0/).

# License
This work is licensed under the MIT License and Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).
	