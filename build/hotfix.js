const fs = require('fs');
const path = require('path');
const util = require('./util');
const moduleJson = util.readModuleJson();

let v = moduleJson.version.split(".");
if (v.length < 3)
	v.push('1');
else
	v[2] = Number(v[2])+1;
moduleJson.version = v.join(".");

fs.writeFileSync(path.join(util.getBaseDir(), "module.json"), JSON.stringify(moduleJson, null, 2));
util.setChangelogVersion(`Hotfix v${moduleJson.version}`)

util.updateUrl();